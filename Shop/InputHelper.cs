﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public class InputHelper
    {
        public string GetString(string message)
        {
            Console.WriteLine(message);
            string result = Console.ReadLine();
            return result;
        }

        public int GetInt(string message)
        {
            int number;
            Console.WriteLine(message);
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("TOnie jest liczba całkowita");
            }
            return number;
        }

        public double GetDouble(string message)
        {
            double number;
            Console.WriteLine(message);
            while (!double.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("To nie jest liczba");
            }
            return number;
        }
    }
}
