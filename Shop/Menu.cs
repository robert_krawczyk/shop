﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shop
{
    public class Menu
    {
        private readonly List<Product> _productList = new List<Product>();

        public void Main()
        {
            var chooce = 0;
            var inputHelper = new InputHelper();

            while(true)
            {
                Console.WriteLine("-------------------------------");
                Console.WriteLine("1. Dodaj produkt");
                Console.WriteLine("2. Zrób listę zakupów");
                Console.WriteLine("3. Koniec");
                chooce = inputHelper.GetInt("Wybierz liczbę z listy");

                switch (chooce)
                {
                    case 1:
                        ProductAdd();
                        break;
                    case 2:
                        Console.WriteLine(TakeProductToList());
                        break;
                    case 3:
                        return;
                    default:
                        Console.WriteLine("Nie ma takiej komendy");
                        break;
                }
            }
        }

        private void ProductAdd()
        {
            var product = new Product();

            product.Name = new InputHelper().GetString("Podaj nazwę produktu");
            product.Price = new InputHelper().GetDouble("Podaj cenę produktu");
            _productList.Add(product);

            new ProductRepository().ProductAddToBase(product);
        }

        private double TakeProductToList()
        {
            var productToChooseList = new ProductRepository().TakeProductsListFromBase();
            double value = 0;

            foreach (var product in productToChooseList)
            {
                Console.WriteLine(product.Id + " " + product.Name + " " + product.Price);
            }

            while (true)
            {
                Console.WriteLine("Wybierz produkt po ID. Wpisz -1 by zakonczyc");

                var itemId = new InputHelper().GetInt("");

                if (itemId == -1)
                {
                    break;
                }

                var quantity = new InputHelper().GetInt("Podaj ilość");

                try
                {
                    value += productToChooseList.Single(p => p.Id == itemId).Price * quantity;
                }
                catch (Exception)
                {
                    Console.WriteLine("Brak takiego produktu");
                }
            }

            return value;
        }
    }
}
