﻿using System.Configuration;
using System.Data.Entity;

namespace Shop
{
    public class ShopDbContexts : DbContext
    {
        public ShopDbContexts() : base(GetConnectionString())
        {
        }

        public DbSet<Product> ProductDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
        }
    }
}