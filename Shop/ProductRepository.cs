﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Shop
{
    public class ProductRepository : DbContext
    {
        public bool ProductAddToBase(Product product)
        {
            using (var dbContext = new ShopDbContexts())
            {
                dbContext.ProductDbSet.Add(product);

                dbContext.SaveChanges();

                return true;

            }
        }

        public List<Product> TakeProductsListFromBase()
        {
            using (var dbContext = new ShopDbContexts())
            {
                var productsList = dbContext.ProductDbSet.ToList();

                return productsList;
            }
        }
    }
}